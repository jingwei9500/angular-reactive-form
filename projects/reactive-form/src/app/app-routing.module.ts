import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'add'
  },
  { 
    path: 'add', 
    loadChildren: () => import('./features/add/add.module').then(m => m.AddModule) 
  }, 
  { 
    path: 'edit', 
    loadChildren: () => import('./features/edit/edit.module').then(m => m.EditModule) 
  }, 
  { 
    path: 'view', 
    loadChildren: () => import('./features/view/view.module').then(m => m.ViewModule) 
  },
  {
    path: '**',
    redirectTo: '',
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
