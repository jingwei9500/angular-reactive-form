import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ChildComponent } from '../child/child.component';



@NgModule({
  declarations: [
    ChildComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports:[
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    ChildComponent
  ]
})
export class SharedModule { }
