
// All forms are required
export interface Resume {
    id: string; // random number of length 6 into string
    name: string; // cannot be less than 3 more than 33
    email: string; // must be email valid
    contact: Contact; // Nested contact object
    skills: Skill[]; // Array of skills
}
  
// Only one required
export interface Contact {
    home_number: number;
    cell_number: number;
    work_number: number;
}

// Must have 1 skill
export interface Skill {
    name: string;
    description: string;
}
