import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Form } from '@angular/forms';
import { Resume } from '../../shared/models/user-info.model';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  //[formGroup] = "form"
  form!: FormGroup 

  toggleButton: boolean = true;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {

    //Initial values
    this.form = this.fb.group({
      id: '',
      name: '',
      email: '',
      contact: this.fb.array([this.fb.control('')]),
      skills: this.fb.array([this.fb.control('')])
    })
  }

  onSubmit(e: any){

    e.preventDefault();

    console.log(this.form.get('contact'));
  }

  get skills(): FormArray{
    return this.form.get('skills') as FormArray
  }

  get contacts(): FormArray{
    return this.form.get('contact') as FormArray
  }

  addSkill(){
    this.skills.push(this.fb.control(''));
  }

  removeSkill(index: number){
    this.skills.removeAt(index);
  }

  addContact(){
    this.contacts.push(this.fb.control(''));
  }

  removeContact(index: number){
    this.contacts.removeAt(index);
  }

  getArray(){
    if(this.contacts.length >= 3){
      this.toggleButton = false;
    }
    else{
      this.toggleButton = true;
    }
  }

}
